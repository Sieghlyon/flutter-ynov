

class Anime{
  final int mal_id;
  final int id;
  final String title;
  final String synopsis;
  final String type;
  final String image_url;
  final int volumes;
  final String end_date;
  final String start_date;

  Anime({this.mal_id, this.id, this.title, this.synopsis, this.type, this.image_url, this.volumes, this.start_date, this.end_date});

  factory Anime.fromJson(Map<String, dynamic> json) {
    return Anime(
      mal_id: json['mal_id'],
      id: json['id'],
      title: json['title'],
      synopsis: json['synopsis'],
      type: json['type'],
      image_url: json['image_url'],
      volumes: json['volumes'],
      start_date: json['start_date'],
      end_date: json['end_date'],
    );
  }

  //EOF
}