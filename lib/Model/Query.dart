

class Query{
  
  String title;
  String type;
  String rated;
  String status;
  String season;
  String limit;

  Query(title, type, {rated = " ", status = "", season ="", limit= ""}){
    this.title = title;
    this.type = type;
    this.rated = rated;
    this.status = status;
    this.season = season;
    this.limit = limit;
  }


}