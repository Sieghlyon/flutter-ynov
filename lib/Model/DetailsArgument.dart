


class DetailsArgument {
  final String id;
  final String description;
  final String name;
  //final String message;

  DetailsArgument(this.id, this.description, this.name);
}