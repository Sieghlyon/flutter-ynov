class Product {
  final double price;
  final int id;
  final String title;
  final String body;
  final String generated_text;
  final List<String> images;
  final List<String> important_badges;
  final List<String> badges;

  Product({this.price, this.id, this.title, this.body, this.generated_text, this.images, this.badges, this.important_badges});

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      price: json['price'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
      generated_text: json['generated_text'],
      images:(json['images'] as List<dynamic>).cast<String>(),
      badges:(json['badges'] as List<dynamic>).cast<String>(),
      important_badges:(json['important_badges'] as List<dynamic>).cast<String>(),
    );
  }

  //EOF
}