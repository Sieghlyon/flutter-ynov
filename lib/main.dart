import 'package:flutter/material.dart';
import './Screen/HomePage.dart';
import './Screen/FormPage.dart';
import './Screen//ListPage.dart';
import './Screen/TestPage.dart';

import 'package:flutter_ynov/Screen/DetailPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      /*theme: ThemeData(
        primarySwatch: Colors.blue,
      ),*/
      //home: MyHomePage(title: 'Flutter Demo Home Page'),
      initialRoute: '/Home',
      routes: <String, WidgetBuilder>{
        '/': (context) => HomePage(),
        '/Form': (context) => FormPage(),
        '/Home': (context) => ListPage(),
        '/Test': (context) => TestPage(),
        DetailPage.routeName: (context) => DetailPage(),
      },
    );
  }
}
