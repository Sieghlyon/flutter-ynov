import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../list/firebase.dart' as fire;
import '../Screen/MainPage.dart';
import 'package:google_sign_in/google_sign_in.dart';

enum FormMode { LOGIN, SIGNUP }
final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;


class FormPage extends StatefulWidget {
  FormPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {

  String _email = "";
  String _password = "";
  FormMode _formMode;
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  String _errorMessage = "";
  bool _isLoading ;


  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      appBar: AppBar(
      
        title: Text("formulaire"),
      ),
      body: Stack(
        children: <Widget>[
          _showBody(),
        ],
      )
      
    );

  }

  Widget _showBody(){
  return new Container(
      padding: EdgeInsets.all(16.0),
      child: new Form(
        key: _formKey,
        child: new ListView(
          shrinkWrap: true,
          children: <Widget>[
            _showEmailInput(),
            _showPasswordInput(),
            _showPrimaryButton(),
            _showSecondaryButton(),
            _showErrorMessage(),
          ],
        ),
      ));
  }

  Widget _showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Email',
            icon: new Icon(
              Icons.mail,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        onSaved: (value) => _email = value,
      ),
    );
  }

  Widget _showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Password',
            icon: new Icon(
              Icons.lock,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
        onSaved: (value) => _password = value,
      ),
    );
  }

  Widget _showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: new MaterialButton(
          elevation: 5.0,
          minWidth: 200.0,
          height: 42.0,
          color: Colors.blue,
          child: _formMode == FormMode.LOGIN
              ? new Text('Login',
                  style: new TextStyle(fontSize: 20.0, color: Colors.white))
              : new Text('Create account',
                  style: new TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: (){
            if (_formKey.currentState.validate()) {
              _formKey.currentState.save();
              _validateAndSubmit();
            }
          },
        ));
  }

  Widget _showSecondaryButton() {
    return new FlatButton(
      child: _formMode == FormMode.LOGIN
          ? new Text('Create an account',
              style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300))
          : new Text('Have an account? Sign in',
              style:
                  new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
      onPressed: _formMode == FormMode.LOGIN
          ? _changeFormToSignUp
          : _changeFormToLogin,
    );
  }

  Widget _showErrorMessage() {
    if (/*_errorMessage.length > 0 &&*/ _errorMessage != null) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 20.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.bold),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  _validateAndSubmit() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String uid;
    if(user != null)
    {uid = user.uid;}
    
    try {
        if (_formMode == FormMode.LOGIN) {
          _auth.signInWithEmailAndPassword(
            email: _email,
            password: _password,
          ).then((user){
            uid = user.uid;

            if(uid == null || uid == ""){
              throw new Exception("uid doesn't exist");
            }
            Navigator.pushNamed(context, '/Home');
            print('Signed in: $user');
          }).catchError((e){
            setState(() {
              print("Readeer error  :  " + e);
              _errorMessage = e.message  ;
            });
          });
        } else {
          _auth.createUserWithEmailAndPassword(
            email: _email,
            password: _password,
          ).then((user){
            uid = user.uid;
            if(uid == null || uid == ""){
              throw new Exception("uid doesn't exist");
            }
            Navigator.pushNamed(context, '/Home');
            print('Signed up user: $user');
          }).catchError((e){
            setState(() {
              _errorMessage = e.message ;
            });
          });
        }
      } catch (e) {
        print('Error: $e');
        setState(() {
          //_isLoading = false;
          _errorMessage = e.message ;
        });
      }
    }

  void _changeFormToSignUp() {
    _formKey.currentState.reset();
    _errorMessage = "";
    setState(() {
      _formMode = FormMode.SIGNUP;
    });
  }

  void _changeFormToLogin() {
    _formKey.currentState.reset();
    _errorMessage = "";
    setState(() {
      _formMode = FormMode.LOGIN;
    });
  }

  //EOF
}
