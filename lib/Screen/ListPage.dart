import 'package:flutter/material.dart';
import 'package:flutter_ynov/list/API.dart';
import 'package:flutter_ynov/Model/Query.dart';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_dart/math/mat2d.dart';
import 'package:flutter_ynov/Screen/DetailPage.dart';
import 'package:flutter_ynov/Model/DetailsArgument.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

List<String> images_list = [];
List<String> description_list = [];
List<String> title_list = [];
List<String> id_list = [];
List<String> type_list = ["tv","ova","movie"];
List<String> status_list = ["airing","completed","upcoming"];

class ListPage extends StatefulWidget {
  ListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  ListPageState createState() => ListPageState();
}

class ListPageState extends State<ListPage> {
  TextEditingController editingController = TextEditingController();
  String type = type_list[0];
  String status;
  String name;
  bool loading = false;


  @override
  Widget build(BuildContext context) {
        
        return Scaffold(
          appBar: AppBar(
            title: Text("Liste of shit"),
          ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _showSearch(),
            Expanded(
              child: _showBody(),
            )
          ]
        )
    );
  }

  _redirection(context) async {
    var user = await _auth.currentUser();
        if(user != null){
          Navigator.pushNamed(context, '/Home');
        }else{
          Navigator.pushNamed(context, '/Form');
        }
  }

  Widget _showSearch(){
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        new Container(
          width: 600,
          height: 80,
          child: TextField(
            style: new TextStyle(
              fontSize: 16.0,
              height: 1.0,
              color: Colors.black,
                                
            ),
            onChanged: (String value) {  
              this.name = value;
            },
            onSubmitted: (value) async {
              this.setState((){
                this.loading = true;
              });
              Query requete = new Query(this.name, this.type, status: this.status);
              var data = await API.fetchAnime(requete);
              
             _updateList(data);
            },
            controller: editingController,
            decoration: InputDecoration(
              labelText: "Search",
              hintText: "Search",
              prefixIcon: Icon(Icons.search),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))
              )
            ),
          ),
        ),
        _listeType(),
        _listeStatus(),
      ]);
  }

  Widget _listeType(){
    return new DropdownButton<String>(
      value: type,
      items: type_list.map((String value) {
        return new DropdownMenuItem<String>(
          
          value: value,
          child: new Text(value),
        );
      }).toList(),
      onChanged: (String val) { 
        setState(() {
          this.type = val;
        });
      },
    );
  }

  Widget _listeStatus(){
    return new DropdownButton<String>(
      value: status,
      items: status_list.map((String value) {
        return new DropdownMenuItem<String>(
          value: value,
          child: new Text(value),
        );
      }).toList(),
      onChanged: (String val) { 
        setState(() {
          this.status = val;
        });
      },
    );
  }
  
  Widget _showBody(){

    if(this.loading == true){
      return _showAnimation();
    }
  return new Container(
      padding: EdgeInsets.all(16.0),
      child: new ListView.builder(
          itemCount: description_list.length,
          itemBuilder: (context, position) {
            return Column(
              children: <Widget>[
                _showColumn(position),
                _showDivider(),
              ]
            );
          }
        )
      );
  }

  Widget _showColumn(position){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        //_showImage(position),
        _showclickable(position),
        //_showSecondaryColumn(position),
        _showPadding(),
      ]
    );
  }

  Widget _showclickable(position){
    return GestureDetector(onTap: () {
        Navigator.pushNamed(
          context,
          DetailPage.routeName,
          arguments: DetailsArgument(
            id_list[position], description_list[position], title_list[position]
          ),
        );
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children:  <Widget>[
          _showImage(position),
          _showSecondaryColumn(position),
        ]
      ),
    );
  }

  Widget _showPadding(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          /*Text(
            "5m",
            style: TextStyle(color: Colors.grey),
          ),*/
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: new IconButton(
              icon : new Icon(
              Icons.star_border,
              size: 35.0,
              color: Colors.grey,
              ),
              onPressed: () {},
            )
          ),
        ],
      ),
    );
  }

  Widget _showSecondaryColumn(position){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[ Padding(
      padding:
        const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0), child: Container(
          width: 450,
          height: 30,
          alignment: Alignment.center,
          child: Text(
          title_list[position], style: TextStyle( fontSize: 22.0, fontWeight: FontWeight.bold), 
          ),
        )
      ),
      Container(height: 150, width: 450,
        child:Text(description_list[position])
        ),
      ],
    ); 
  }

  Widget _showImage(position){
    return Padding(
      padding:
        const EdgeInsets.fromLTRB(12.0, 3.0, 12.0, 3.0), child:Image.network(
          images_list[position], width: 170, height: 170, scale: 1, alignment: Alignment(0, 0),
        )
    );
  }

  Widget _showDivider(){
    return Divider(
      height: 2.0,
      color: Colors.grey,
    );
  }

  _updateList(animes){
    List<String> titles = new List<String>();
    List<String> descri = new List<String>();
    List<String> images = new List<String>();
    List<String> ids = new List<String>();

    titles.clear();
    descri.clear();
    images.clear();
    ids.clear();

    for(var i = 0; i < animes.length; i++){
      titles.add(animes[i].title);
      descri.add(animes[i].synopsis);
      images.add(animes[i].image_url);
      ids.add(animes[i].mal_id.toString());
    }

    this.setState((){
      title_list = titles;
      description_list = descri;
      images_list = images;
      id_list = ids;
      this.loading = false;
    });

  }

  Widget _showAnimation() {
    return new FlareActor("animations/Spinning Dots Loader.flr",
      alignment: Alignment.center,
      //fit: BoxFit.contain,
      animation: "play");
  }

  //EOF
}