import 'package:flutter/material.dart';
import 'package:flutter_ynov/Model/DetailsArgument.dart';
import 'package:flutter_ynov/list/API.dart';

class DetailPage extends StatefulWidget {
  DetailPage({Key key}) : super(key: key);

  static const routeName = '/Detail';

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {

  List<Widget> widgets = new List<Widget>();

  @override
  Widget build(BuildContext context) {
    final DetailsArgument args = ModalRoute.of(context).settings.arguments;

    _showPicturesList(args.id.toString());

    return Scaffold(
      appBar: AppBar(
        title: Text("Details"),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _showTitle(args.name),
            Expanded(
              child: _managaScrolling(args.id.toString(), args.description),
            ),
            //_showPictures(args.id.toString()),
            //_showDescription(args.description),
          ]
        ),
    );
  }

  Widget _managaScrolling(id, description){
    return new Container(
      padding: EdgeInsets.all(16.0),
      child: new ListView.builder(
          itemCount: 1,
          itemBuilder: (context, position) {
            return Column(
              children: <Widget>[
                _showPictures(id),
                _showDescription(description),
              ]
            );
          }
        )
      );
  }

  Widget _showTitle(title){
    return new Text(title, textAlign: TextAlign.center,style: TextStyle(fontSize: 26.0,fontWeight: FontWeight.bold));
      
  }

  Widget _showDescription(description){
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container( width: 800,
          child:Text(description, textAlign: TextAlign.center,style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.normal))
        ),
      ]
    );
  }

  Widget _showPictures(id){
    return new Container(
      margin: EdgeInsets.symmetric(vertical:5.0),
      height:250,
      child:ListView(
        scrollDirection: Axis.horizontal,
        children: this.widgets,
      ) 
    );
  }

   _showPicturesList(id) {
    List<Widget> liste_w = new List<Widget>();

    API.getPictures(id).then((res)=>{
      res.forEach((elem)=>{
        liste_w.add(Container(
            width: 250,
            child: Card(
              child: Wrap(
                children: <Widget>[
                  Image.network(elem),
                ]
              ),
            ),
          )
        )
      }),

      this.setState((){
        this.widgets = liste_w;
      })
    });
  }

  _showMoreinfos(){

  }

  _showEpisodes(){

  }
  
}