import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    _redirection(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Second Screen"),
      ),
      body:  Stack(
        children: <Widget>[
          _showButton(context),
          _showButtonTest(context),
        ],
      )
    );
  }

   _redirection(context) async {
    var user = await _auth.currentUser();
        if(user != null){
          Navigator.pushNamed(context, '/Home');
        }else{
          Navigator.pushNamed(context, '/Form');
        }
  }

  _showButton(context){
    return Center(
      child: RaisedButton(
        onPressed: () async {
        var user = await _auth.currentUser();
        if(user != null){
          Navigator.pushNamed(context, '/Home');
        }else{
          Navigator.pushNamed(context, '/Form');
        }
          
        },
          child: Text('GO to form'),
      )
    );
  }

  _showButtonTest(context){
    return Container(
      child: RaisedButton(
        onPressed: () async {
          Navigator.pushNamed(context, '/Test');
        },
          child: Text('Test page'),
      )
    );
  }


}