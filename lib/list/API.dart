import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../Model/Anime.dart';

class API {

    static final basicURL = "https://api.jikan.moe/v3/search/";
    static final basicURL2 = "https://api.jikan.moe/v3/anime/";

    static Future<List<Anime>> fetchAnime(query) async {
    
    final URL = basicURL + "anime?q=" + query.title.replaceAll(" ", "+") + "&" +
                (query.status != null ? "status=" + query.status + "&" : "") +
                (query.type != null ? "type=" + query.type + "&" : "") +
                "rated=R17&rated=g&rated=pg&rated=pg13" ;

                //(query.rated ? "rated=" + query.rated + "&" : "") +
                //(query.limit != null ? "limit=" + query.limit + "&" : "") ;
    
    final response =
        await http.get(URL, headers: {"Content-Type": "application/x-www-form-urlencoded"})
        .catchError((onError){
          throw Exception(onError.toString());
        });

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON
      var listeAnimes = json.decode(response.body);
      
      List<Anime> animes = new List<Anime>();
      for(var i = 0; i < listeAnimes["results"].length ; i++){
        animes.add(Anime.fromJson(listeAnimes["results"][i]));
      }

      return animes;
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post' + response.body.toString() + " URL = " + URL);
      
    }
  }

  static Future<List<String>> getPictures(id) async {
    final URL = basicURL2 + id + "/pictures";
    
    final response =
        await http.get(URL, headers: {"Content-Type": "application/x-www-form-urlencoded"})
        .catchError((onError){
          throw Exception(onError.toString());
        });

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON
      var listeP = json.decode(response.body);
      
      List<String> pictures = new List<String>();
      for(var i = 0; i < listeP["pictures"].length ; i++){
        pictures.add(listeP["pictures"][i]["small"]);
      }

      return pictures;
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post' + response.body.toString() + " URL = " + URL);
      
    }
  }

}

